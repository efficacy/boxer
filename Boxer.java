import java.util.Collections;
import java.util.ArrayList;
import java.util.List;

class Box implements Comparable<Box> {
  public int x;
  public int y;
  public int z;

  public Box(int x, int y,int z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  public int size() {
    return this.x * this.y * z;
  }

  public boolean fitsOn(Box pile) {
    return (this.x * 1.1) < pile.x && (this.y * 1.1) < pile.y && (this.z * 1.5) < pile.z;
  }

  public String toString() {
    return "(" + x + "," + y + "," + z + ")";
  }

  @Override
  public int compareTo(Box other) {
    return other.size() - this.size();
  }
}

public class Boxer {
  public static List<Box> boxes = new ArrayList<Box>();
  public static List<Box> piles = new ArrayList<Box>();

  public static void main(String args[]) {
    boxes.add(new Box(4,3,2));
    boxes.add(new Box(5,4,4));
    boxes.add(new Box(4,3,5));
    boxes.add(new Box(2,2,2));

    Collections.sort(boxes);

    for (Box box : boxes) {
      Integer best = null;
      for (int i = 0; i < piles.size(); ++i) {
        Box pile = piles.get(i);
        if (box.fitsOn(pile)) {
          if (null == best || pile.size() < piles.get(best).size()) {
            best = i;
          }
        }
      }
      if (null != best) {
        System.out.println("adding " + box + " to pile " + best);
        piles.set(best, box);
      } else {
        System.out.println("adding " + box + " to new pile " + piles.size());
        piles.add(box);
      }
    }

    System.out.println("solution uses " + piles.size() + " piles");
  }
}
