var util = require('util');

function size(box) {
  return box.x * box.y * box.z;
}
function fits(box, pile) {
  return (box.x * 1.1) < pile.x && (box.y * 1.1) < pile.y && (box.z * 1.5) < pile.z;
}

var boxes = require('./set1');
var piles = [];

boxes.sort(function(a,b) { return size(a) < size(b); });

boxes.forEach(function(box) {
  var best = null;
  piles.forEach(function(pile, i) {
    if (fits(box, pile)) {
      if (null == best || size(pile) < size(best)) {
        best = i;
      }
    }
  });
  if (best != null) {
    console.log('adding ' + util.inspect(box) + ' to pile ' + best);
    piles[best] = box;
  } else {
    console.log('adding ' + util.inspect(box) + ' to new pile ' + piles.length);
    piles.push(box);
  }
});

console.log('solution uses ' + piles.length + ' piles');